#!/usr/bin/env python
"""
view git repository log info
"""

import re
import pygit2

#list all branch

def allBranches(repoPath=''):
    repo = pygit2.Repository(repoPath)
    regex = re.compile('^refs/heads/')
    return  filter(lambda r:regex.match(r),repo.listall_references())

def createRepo(repoPath='',isBare=False):
    print pygit2.init_repository(repoPath,isBare)

def show(repoPath='',objectInfo=''):
    repo = pygit2.Repository(repoPath)
    commit = repo.revparse_single(objectInfo)
    print commit.message,commit.committer.name,commit.hex
    for e in commit.tree:
        print e.name

def checkout(repoPath='',workspace=''):
    repo = pygit2.Repository(repoPath)
    repo.checkout()

def clone(url='',path='',bare=False,remote_name='tmp',checkout_branch="V1.0"):
    pygit2.clone_repository(url,path,bare,remote_name)
if __name__ == '__main__':

    #branches = allBranches('/home/us/repository/test.git')
    #createRepo('/home/us/repository/test02.git',True)
    #show('/home/us/repository/test.git','a2d1176aa25')
    clone('http://git.oschina.net/changjie-fan/GitClient.git','/home/us/tmp/clone.git')
